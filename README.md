## Purpose of this tool 

The tool exists to facilitate one of the Release readiness checklist action item: Manifest Check. (See [Software Release Principles](https://www.jamasoftware.com/blog/release-management-jama/)).

There are two things to check:

1. Verify that all the JIRA tickets that taget the version to release, has an associate branch that is merged.
2. Verify that all the branches merged into the release branch, has a JIRA ticket that bears the release version.

For point number 1, JIRA already has a tool to give a summary. However the second point does not exist in JIRA. This little python project fills in that blank.

---

## Usage

`usage: main.py [-h] -c1 Commit 1 [-c2 Commit 2, default is HEAD]`         
`		[-conf Configuration file]`              
`		[-e Excluding project ID, i.e. ASDK)] -p Jira project code.`
`		(i.e. CORE, DIR -v Version to release.`
`the following arguments are required: -c1, -p, -v`

