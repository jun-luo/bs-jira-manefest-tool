from typing import List
from entity import *
import subprocess


def parse_pulls(pull_lines):
    pulls = []
    error_file_name = "error.txt"
    error_file = open(error_file_name, mode='w')

    for pull_line in pull_lines:
        try:
            pull = Pull(pull_line[0], pull_line[1])
            pulls.append(pull)
        except Exception as exception:
            error_file.write(str(exception) + "\n")
    return pulls


def parse_branches(branch_lines):
    branches = []
    for branch_line in branch_lines:
        branch = Branch(branch_line[0], branch_line[1])
        branches.append(branch)
    return branches


def parse_commits(commit_lines):
    commits = []
    for commit_line in commit_lines:
        commit = Commit(commit_line[0], commit_line[1])
        commits.append(commit)
    return commits


def get_jira_commits(commit1: str, commit2: str, mode: str, folder: str, project_dir: str):
    try:
        command = 'git log '
        if mode != "long":
            command += '--merges '
        command += commit1 + ".." + commit2 + " "
        if folder:
            command += "-- " + folder

        print(command)
        p = subprocess.run(command.split(), cwd=project_dir, stdout=subprocess.PIPE)
        output = p.stdout
    except subprocess.CalledProcessError as exception:
        print("return code", exception.returncode, "check your command")

    return output.decode().splitlines()
