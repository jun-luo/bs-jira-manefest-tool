import http.client
from entity import Config


def make_request(id: str, config: Config, method: str):
    headers = {'Authorization': 'Basic ' + config.jira_user_base64_auth, 'Content-Type': 'application/json'}

    conn = http.client.HTTPSConnection(config.jira_hostname)

    conn.request(method=method, url=config.jira_issue_url + id, headers=headers)
    response = conn.getresponse()
    return response.read()
