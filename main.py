#!/usr/bin/python3

import argparse, git_helper
from functions import *


parser = argparse.ArgumentParser()
parser.add_argument("-c1", type=str, metavar='Commit 1', required=True)
parser.add_argument("-c2", type=str, default='HEAD', metavar='Commit 2, default is HEAD')
parser.add_argument("-mode", type=str, default='long', metavar='mode: long or quick, default is quick')
parser.add_argument("-conf", type=str, default='config.json', metavar='Configuration file')
parser.add_argument("-folder", type=str, metavar='The source folder to limit scope, relative to the project dir in the conf file. (i.e. front-end folder only)')
parser.add_argument("-e", type=str, metavar='Excluding project ID or version, (i.e. "ASDK,Tizen")')

args = parser.parse_args()
excludings = str(args.e).split(',')

print("loading configuration file ...")
with open(args.conf) as config_file:
    config = Config(json.load(config_file))

print("loading all commits ...")
lines = git_helper.get_jira_commits(args.c1, args.c2, args.mode, args.folder, config.project_dir)
(merge_pulls, merge_branches, commit_lines) = separate_commit_types(lines, args.mode)

print("parsing all %d merge commits ..." % (len(merge_pulls)))
commits = git_helper.parse_pulls(merge_pulls)
print("parsing all %d merged branches ..." % (len(merge_branches)))
branches = git_helper.parse_branches(merge_branches)

if args.mode == 'long':
    print("parsing all %d regular commits ..." % (len(commit_lines)))
    commits.extend(git_helper.parse_commits(commit_lines))

print("sorting ...")
sorted_and_unique_commits = sort_unique_filter(commits, excludings)
sorted_and_unique_branches = sort_unique_filter(branches, excludings)

process_branches(sorted_and_unique_branches)
process_commits(sorted_and_unique_commits, config, excludings)

print("total of %d merges, of which, %d branch merges, %d PRs" % (len(merge_branches)+len(merge_pulls), len(merge_branches), len(merge_pulls)))
print("total of %d unique branches merged without jira ID and %d unique issues merged" % (len(sorted_and_unique_branches), len(sorted_and_unique_commits)))
