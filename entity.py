import re, json


class Commit(object):
    commit: str = None
    original: str = None
    from_: str = None
    from_issue_type: str = None
    id_format = "[a-zA-Z]{2,5}-[0-9X]+"

    def __init__(self, commit_line, summary_line):
        def find_jira_ids(to_match: str):
            search_regex = re.compile(Commit.id_format)
            return search_regex.findall(to_match)
        self.original = summary_line
        self.commit = commit_line
        jira_ids = find_jira_ids(summary_line)
        if not jira_ids:
            raise Exception('Error: unable to locate Jira ID in \'%s\', merged at %s' % (pull_line, commit_line))
        size = len(jira_ids)
        if size >= 1:
            self.from_ = jira_ids[0]


class Merge(Commit):
    to_: str = None


class Pull(Merge):
    def __init__(self, commit_line, pull_line):
        def find_jira_ids(to_match: str):
            search_regex = re.compile(Merge.id_format)
            return search_regex.findall(to_match)

        self.original = pull_line
        self.commit = commit_line
        jira_ids = find_jira_ids(pull_line)

        if not jira_ids:
            raise Exception('Error: unable to locate Jira ID in \'%s\', merged at %s' % (pull_line, commit_line))
        size = len(jira_ids)
        if size == 1:
            self.from_ = jira_ids[0]
        if size == 2:
            self.from_ = jira_ids[0]
            self.to_ = jira_ids[1]


class Branch(Merge):
    def __init__(self, commit_line, branch_line):
        def find_remote_branch(to_match: str):
            search_string = "'.+'"
            search_regex = re.compile(search_string)
            return search_regex.findall(to_match)

        self.original = branch_line
        self.commit = commit_line
        branch = find_remote_branch(branch_line)
        if len(branch) == 1:
            self.from_ = branch[0]


class Config(object):
    project_dir: str
    jira_hostname: str
    jira_issue_url: str
    jira_user_base64_auth: str
    jira_user_api_token_auth: str
    parent_field_name: str

    def __init__(self, config: json):
        self.project_dir = config['project-dir']
        self.jira_hostname = config['jira-hostname']
        self.jira_issue_url = config['jira-issue-url']
        self.jira_user_base64_auth = config['jira-user-base64-auth']
        if 'parent-field-name' in config:
            self.parent_field_name = config['parent-field-name']
        else:
            self.parent_field_name = None
