from typing import List
from entity import *
import re, http_helper, json, sys, collections


def separate_commit_types(lines: List[str], mode: str):
    merge_pulls = []
    merge_branches = []
    commits = []

    def is_commit_line(line: str):
        pattern = '^commit .{40}'
        regex = re.compile(pattern)
        return regex.findall(line)

    curr_commit = ''

    for line in lines:
        if is_commit_line(line):
            curr_commit = line[7:]
        if 'merge pull ' in line.lower() or 'merged in ' in line.lower():
            merge_pulls.append((curr_commit, line))
            continue
        if 'merge branch ' in line.lower() or 'merge remote ' in line.lower():
            merge_branches.append((curr_commit, line))
            continue
        if mode == 'long' and is_commit_summary(line):
            commits.append((curr_commit, line))
            continue

    return merge_pulls, merge_branches, commits


def is_commit_summary(string: str):
    regex = re.compile(Commit.id_format)
    return len(regex.findall(string)) != 0


def should_exclude(exclude_list: List[str], string: str):
    for exclude in exclude_list:
        if exclude in string:
            return True
    return False


def should_exclude_list(exclude_list: List[str], strings: List[str]):
    for string in strings:
        if should_exclude(exclude_list, string):
            return True
    return False


def sort_unique_filter(commits: List[Commit], excluding_list: List[str] = list()):
    def get_from(commit: Commit):
        return commit.from_

    unique_commits = list()
    unique_jira_id = set()
    for commit in commits:
        if not commit.from_:
            continue
        if commit.from_ not in unique_jira_id and (not should_exclude(excluding_list, commit.from_)):
            unique_jira_id.add(commit.from_)
            unique_commits.append(commit)

    return sorted(unique_commits, key=get_from)


def verify_id_format(id_to_check: str):
    search_string = Merge.id_format
    search_regex = re.compile(search_string)
    return search_regex.match(id_to_check)


def process_branches(branches: List[Merge]):
    with open('merged-branches.txt', mode='w') as output_branches:
        for branch in branches:
            output_branches.write("branch %s is merged, at commit %s\n" % (branch.from_, branch.commit))


def friendly_collection_str(collection):
    return str(collection) if collection else 'none'


def output_data(file, from_, summary, issue_type, components, fix_versions, parent, parent_fix_versions, status,
                commit):
    file.write("%s| %s| %s| %s| %s| %s| %s| %s| %s\n" % (
        from_, summary, issue_type, components, fix_versions, parent, parent_fix_versions, status, commit))


def process_commits(commits: List[Commit], config: Config, excluding_list: List[str] = list()):
    def get_details(json_data: json):
        def get_fields(json_data: json, field_name: str):
            fields = set()
            for field in json_data['fields'][field_name]:
                fields.add(field['name'])
            return fields

        status = json_data['fields']['status']['name']
        summary = json_data['fields']['summary']
        issue_type = json_data['fields']['issuetype']['name']
        versions = get_fields(json_data, 'fixVersions')
        components = get_fields(json_data, 'components')

        if config.parent_field_name is not None and config.parent_field_name in json_data['fields']:
            parent = json_data['fields'][config.parent_field_name]
            return versions, summary, issue_type, components, status, parent
        else:
            return versions, summary, issue_type, components, status, None

    output_file = open('merged-prs.txt', mode='w')
    output_data(output_file, 'ID', 'Summary', 'Type', 'Components',
                'Fix Versions', 'Parent',
                'Parent Versions', 'Status', 'Merge Hash')

    id_format = re.compile(Merge.id_format)

    for commit in commits:
        if commit.from_:
            from_ = commit.from_
            if not verify_id_format(from_):
                sys.stderr.write("Error: %s is not a valid jira id, original line is %s \n" % (from_, commit.original))
                continue

            response_data = http_helper.make_request(from_, method='GET', config=config)
            jdata = json.loads(response_data)
            try:
                (fix_versions, summary, issue_type, components, status, parent) = get_details(jdata)

                if should_exclude_list(excluding_list, fix_versions):
                    continue

                if parent:
                    if id_format.fullmatch(str(parent)):
                        response_parent = http_helper.make_request(parent, method='GET', config=config)
                    else:
                        response_parent = http_helper.make_request(parent['key'], method='GET', config=config)

                    jdata_parent = json.loads(response_parent)
                    (parent_versions, parent_summary, parent_type, parent_components, parent_status,
                     grand_parent) = get_details(
                        jdata_parent)
                    parent_key = jdata_parent['key']

                else:
                    parent_key = None
                    parent_versions = []

                output_data(output_file, from_, summary, issue_type, friendly_collection_str(components),
                            friendly_collection_str(fix_versions), parent_key,
                            friendly_collection_str(parent_versions), status, commit.commit)

            except Exception as ex:
                sys.stderr.write("Error %s: data is '%s', merge is '%s'\n" % (ex, jdata, commit.original))

        else:
            sys.stderr.write("Error: %s does not contain 'merge from'\n" % commit.original)

    output_file.close()
